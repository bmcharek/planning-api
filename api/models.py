import datetime

from django.db import models
from django.contrib.auth import get_user_model


class Member(models.Model):
    GENDER_LIST = (('MALE', 'MALE'),
                   ('FEMALE', 'FEMALE'))
    PLAN_LIST = (('SESSION', 'SESSION'),
                 ('MONTH', 'MONTH'),
                 ('YEAR', 'YEAR'))
    member_id = models.AutoField(primary_key=True)
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    department = models.CharField(max_length=100)
    date_joined = models.DateField(null=True)
    gender = models.CharField(
        max_length=8,
        choices=GENDER_LIST,
        default='MALE'
    )
    weight = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    remarks = models.TextField(blank=True, null=True)
    plan = models.CharField(
        max_length=10,
        choices=PLAN_LIST,
        default='SESSION'
    )

    class Meta:
        db_table = u'member'


class Excercise(models.Model):
    name = models.CharField(unique=True, max_length=50)
    description = models.TextField(blank=True, null=True)
    # media = models.ImageField(upload_to='img', null=True)
    EXCERCISE_INTENSITIES = (
        ('EASY', 'EASY'),
        ('MEDIUM', 'MEDIUM'),
        ('ADVANCED', 'ADVANCED'),
    )
    teacher = models.CharField(null=True, max_length=50)
    intensity = models.CharField(
        max_length=10,
        choices=EXCERCISE_INTENSITIES,
        default='EASY'
    )
    level = models.CharField(
        max_length=10,
        choices=EXCERCISE_INTENSITIES,
        default='EASY'
    )
    equipment = models.CharField(null=True, max_length=50)
    burned_calories = models.IntegerField(default=0)
    duration = models.TimeField(default=datetime.time(00, 16, 00))
    distance = models.FloatField(null=True)

    class Meta:
        db_table = u'excercise'


class Day(models.Model):
    name = models.CharField(unique=True, max_length=50)
    description = models.TextField(blank=True, null=True)
    workouts = models.ManyToManyField(Excercise)
    date = models.DateField(null=True)

    class Meta:
        db_table = u'day'


class Plan(models.Model):
    name = models.CharField(unique=True, max_length=50)
    created_by = models.CharField(null=True, max_length=50)
    description = models.TextField(blank=True, null=True)
    goal = models.CharField(blank=True, max_length=200)
    start_date = models.DateField(null=True)
    frequency = models.IntegerField(default=0)
    duration = models.IntegerField(default=0)
    repeated = models.IntegerField(default=0)
    days = models.ManyToManyField(Day)
    # should become members
    assigned_to = models.ManyToManyField(get_user_model())

    class Meta:
        db_table = u'plan'