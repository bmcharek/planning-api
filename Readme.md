========
Planning
========

Planning is a simple Django API to conduct Web-based polls. For each
question, visitors can choose between a fixed number of answers.

Detailed documentation is in the "docs" directory.

API
---

1. Create virtual environment: `virualenv venv`

2. Activate virtualenv: `source ven/bin/activate`

3. Install requirements: `pip install -r planning_api/requirements.txt` 

3. Copy params_example.py to params.py and set your DB paramteres. Make sure you have mysql installed and created the 
   proper database for the project. 

3. Run `python manage.py migrate` to create the planning models.

3. Run `python manage.py createsuperuser` to create a superuser.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a planning_poll (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/planning_api/ to participate in the planning.