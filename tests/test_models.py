import pytest
from django.test import TestCase

from django.contrib.auth import get_user_model
import pytest
from planning_api.api.models import *
from .factories import *


User = get_user_model()
# @pytest.mark.django_db
# def test_create_member():
#     """
#     Test if Member is created
#     Returns:
#
#     """
#     user = UserFactory.create()
#     member = MemberFactory.create(user=user)
#     assert member.user == user


@pytest.mark.django_db()
def test_exercise_model():
    exercise = ExerciseFactory.create(name='push up')
    assert exercise.name == 'push up'


@pytest.mark.django_db()
def test_day_model():
    exercise = ExerciseFactory.create(name='push up')
    day = DayFactory.create(name='arms day',
                                  workouts=[exercise])

    assert day.name == 'arms day'


@pytest.mark.django_db()
def test_plan_model():
    exercise = ExerciseFactory.create(name='push up')
    day = DayFactory.create(name='arms day',
                            workouts=[exercise])
    plan = PlanFactory.create(name='good plan',
                              days=[day])

    assert plan.name == 'good plan'


@pytest.mark.django_db()
def test_member_model():
    user = UserFactory.create(id=1)
    member = MemberFactory.create(user=user)
    assert user.id == member.user_id